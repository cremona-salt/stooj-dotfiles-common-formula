# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set sls_user_present = tplroot ~ '.user.user' %}

include:
  - {{ sls_user_present }}

stooj-dotfiles-common-directories-local-share-directory-present:
  file.directory:
    - name: {{ stooj_dotfiles_common.directories.local_share_dir }}
    - user: stooj
    - group: stooj
    - mode: 755
    - makedirs: True
    - require:
      - sls: {{ sls_user_present }}
