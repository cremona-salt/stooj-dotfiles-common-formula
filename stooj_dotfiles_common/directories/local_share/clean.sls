# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

stooj-dotfiles-common-directories-local-share-clean-directory-absent:
  file.absent:
    - name: {{ stooj_dotfiles_common.directories.local_share_dir }}
