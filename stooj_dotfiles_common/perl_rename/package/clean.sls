# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

stooj-dotfiles-common-perl_rename-package-clean-tpm-absent:
  file.absent:
    - name: /home/stooj/.perl_rename/plugins/tpm

stooj-dotfiles-common-perl_rename-package-clean-stock-config-dir-absent:
  file.absent:
    - name: /home/stooj/.perl_rename

stooj-dotfiles-common-perl_rename-package-clean-package-absent:
  pkg.removed:
    - pkgs: {{ stooj_dotfiles_common.perl_rename.pkgs | yaml }}
