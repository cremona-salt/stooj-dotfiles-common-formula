# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}
{%- set sls_zsh_config_present = tplroot ~ '.zsh.config.rc' %}
{%- set sls_user_present = tplroot ~ '.user.user' %}

stooj-dotfiles-common-perl-rename-package-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles_common.perl_rename.pkgs | yaml }}
