# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubroot = tplroot ~ '.' ~ tplsubname %}
{%- set sls_user_present = tplroot ~ '.user.user' %}
{%- set sls_zsh_config_present = tplroot ~ '.zsh.config.rc' %}

include:
  - {{ sls_user_present }}
  - {{ sls_zsh_config_present }}

stooj-dotfiles-common-perl-rename-config-file-rc-includes-zsh-managed:
  file.managed:
    - name: /home/stooj/.config/zsh/rc_includes/perl-rename.zsh
    - source: {{ files_switch(['perl-rename.zsh.tmpl'],
                              lookup='stooj-dotfiles-common-perl-rename-config-file-rc-includes-zsh-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        perl_rename: {{ stooj_dotfiles_common.perl_rename | json }}
    - require:
      - sls: {{ sls_user_present }}
      - sls: {{ sls_zsh_config_present }}
