# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}
{%- set sls_user_present = tplroot ~ '.user.user' %}

include:
  - {{ sls_user_present }}

stooj-dotfiles-common-cig-package-install-pkg-installed:
  file.managed:
    - name: /home/stooj/bin/cig
    - source: {{ stooj_dotfiles_common.cig.source }}
    - source_hash: {{ stooj_dotfiles_common.cig.source_hash }}
    - user: stooj
    - group: stooj
    - mode: 0700
    - makedirs: True
    - require:
      - sls: {{ sls_user_present }}
