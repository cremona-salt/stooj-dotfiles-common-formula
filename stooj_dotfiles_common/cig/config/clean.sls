# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-common-cig-config-clean-cig-config-absent:
  file.absent:
    - name: /home/stooj/.config/cig/cig.conf

stooj-dotfiles-common-cig-config-clean-cig-config-dir-absent:
  file.absent:
    - name: /home/stooj/.config/cig
