# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubroot = tplroot ~ '.' ~ tplsubname %}
{%- set sls_user_present = tplroot ~ '.user.user' %}

include:
  - {{ sls_user_present }}

stooj-dotfiles-common-cig-config-file-config-managed:
  file.managed:
    - name: /home/stooj/.cig.yaml
    - source: {{ files_switch(['cig.yaml.tmpl'],
                              lookup='stooj-dotfiles-common-cig-config-file-managed',
                              use_subpath=True
                             )
              }}
    - mode: 600
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        cig: {{ stooj_dotfiles_common.cig | json }}
    - require:
      - sls: {{ sls_user_present }}
