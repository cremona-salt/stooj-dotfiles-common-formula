# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubroot = tplroot ~ '.' ~ tplsubname %}
{%- set sls_config_clean = tplsubroot ~ '.config.clean' %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

include:
  - {{ sls_config_clean }}

stooj-dotfiles-common-zsh-package-clean-pkg-removed:
  pkg.removed:
    - pkgs: {{ stooj_dotfiles_common.zsh.pkgs | yaml }}
    - require:
      - sls: {{ sls_config_clean }}

stooj-dotfiles-common-zsh-package-install-antibody-symlink-absent:
  file.absent:
    - name: /home/stooj/bin/antibody

stooj-dotfiles-common-zsh-package-install-antibody-absent:
  file.absent:
    - name: /home/stooj/.local/share/antibody
