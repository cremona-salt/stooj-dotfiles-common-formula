# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}
{%- set sls_bin_directory_create = tplroot ~ '.directories.bin.directory' %}
{%- set sls_local_share_directory_create = tplroot ~ '.directories.local_share.directory' %}

include:
  - {{ sls_bin_directory_create }}
  - {{ sls_local_share_directory_create }}

stooj-dotfiles-common-zsh-package-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles_common.zsh.pkgs | yaml }}

{%- set antibody_version = stooj_dotfiles_common.zsh.antibody.version %}
stooj-dotfiles-common-zsh-package-install-antibody-installed:
  archive.extracted:
    - name: /home/stooj/.local/share/antibody
    - source: https://github.com/getantibody/antibody/releases/download/v{{ antibody_version }}/antibody_Linux_x86_64.tar.gz
    - source_hash: https://github.com/getantibody/antibody/releases/download/v{{ antibody_version }}/antibody_{{ antibody_version }}_checksums.txt
    - user: stooj
    - group: stooj
    - enforce_toplevel: False
    - require:
      - sls: {{ sls_local_share_directory_create }}

stooj-dotfiles-common-zsh-package-install-antibody-symlink-managed:
  file.symlink:
    - name: /home/stooj/bin/antibody
    - target: /home/stooj/.local/share/antibody/antibody
    - user: stooj
    - group: stooj
    - mode: 755
    - require:
      - stooj-dotfiles-common-zsh-package-install-antibody-installed
      - sls: {{ sls_bin_directory_create }}
