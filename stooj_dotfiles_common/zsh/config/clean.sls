# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

stooj-dotfiles-common-zsh-config-clean-env-entry-config-file-absent:
  file.absent:
    - name: {{ stooj_dotfiles_common.zsh.env_entry_config }}

stooj-dotfiles-common-zsh-config-clean-env-config-file-absent:
  file.absent:
    - name: {{ stooj_dotfiles_common.zsh.zdotdir }}/{{ stooj_dotfiles_common.zsh.env_config }}

stooj-dotfiles-common-zsh-config-clean-env-functions-file-absent:
  file.absent:
    - name: {{ stooj_dotfiles_common.zsh.zdotdir }}/{{ stooj_dotfiles_common.zsh.env_functions }}

{%- set env_includes_dir = stooj_dotfiles_common.zsh.env_includes_dir %}
{% for name in stooj_dotfiles_common.zsh.env_include_files %}
stooj-dotfiles-common-zsh-config-clean-env-include-{{ name }}-managed:
  file.absent:
    - name: {{ stooj_dotfiles_common.zsh.zdotdir }}/{{ env_includes_dir }}/{{ name }}
{% endfor %}

stooj-dotfiles-common-zsh-config-clean-env-include-directory-absent:
  file.absent:
    - name: {{ stooj_dotfiles_common.zsh.zdotdir }}/{{ env_includes_dir }}

stooj-dotfiles-common-zsh-config-clean-rc-config-file-absent:
  file.absent:
    - name: {{ stooj_dotfiles_common.zsh.zdotdir }}/{{ stooj_dotfiles_common.zsh.rc_config }}

stooj-dotfiles-common-zsh-config-clean-profile-config-file-absent:
  file.absent:
    - name: {{ stooj_dotfiles_common.zsh.zdotdir }}/{{ stooj_dotfiles_common.zsh.profile_config }}

stooj-dotfiles-common-zsh-config-clean-logout-config-file-absent:
  file.absent:
    - name: {{ stooj_dotfiles_common.zsh.zdotdir }}/{{ stooj_dotfiles_common.zsh.logout_config }}

stooj-dotfiles-common-zsh-config-clean-zdotdir-directory-absent:
  file.absent:
    - name: {{ stooj_dotfiles_common.zsh.zdotdir }}
