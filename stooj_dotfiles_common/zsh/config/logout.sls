# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_bootstrap = tplroot ~ '.zsh.config.bootstrap' %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_config_bootstrap }}

stooj-dotfiles-common-zsh-config-logout-config-managed:
  file.managed:
    - name: {{ stooj_dotfiles_common.zsh.zdotdir }}/{{ stooj_dotfiles_common.zsh.logout_config }}
    - source: {{ files_switch(['zlogout.tmpl'],
                              lookup='stooj-dotfiles-common-zsh-config-logout-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - require:
      - sls: {{ sls_config_bootstrap }}
    - context:
        zsh: {{ stooj_dotfiles_common.zsh | json }}
