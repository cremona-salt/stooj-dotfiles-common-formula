# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set sls_user_present = tplroot ~ '.user.user' %}

include:
  - {{ sls_user_present }}

stooj-dotfiles-common-zsh-config-file-env-entry-config-managed:
  file.managed:
    - name: {{ stooj_dotfiles_common.zsh.env_entry_config }}
    - source: {{ files_switch(['zshenv-base.tmpl'],
                              lookup='stooj-dotfiles-common-zsh-config-file-env-entry-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        zsh: {{ stooj_dotfiles_common.zsh | json }}
    - require:
      - sls: {{ sls_user_present }}

stooj-dotfiles-common-zsh-config-file-zdotdir-managed:
  file.directory:
    - name: {{ stooj_dotfiles_common.zsh.zdotdir }}
    - user: stooj
    - group: stooj
    - mode: 755
    - makedirs: True
    - require:
      - sls: {{ sls_user_present }}

stooj-dotfiles-common-zsh-config-file-local-share-dir-managed:
  file.directory:
    - name: /home/stooj/.local/share/zsh
    - user: stooj
    - group: stooj
    - mode: 755
    - makedirs: True
    - require:
      - sls: {{ sls_user_present }}
