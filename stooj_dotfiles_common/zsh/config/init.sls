# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .bootstrap
  - .env
  - .profile
  - .rc
  - .logout
