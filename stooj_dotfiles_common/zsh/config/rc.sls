# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_bootstrap = tplroot ~ '.zsh.config.bootstrap' %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_config_bootstrap }}

stooj-dotfiles-common-zsh-config-rc-config-managed:
  file.managed:
    - name: {{ stooj_dotfiles_common.zsh.zdotdir }}/{{ stooj_dotfiles_common.zsh.rc_config }}
    - source: {{ files_switch(['zshrc.tmpl'],
                              lookup='stooj-dotfiles-common-zsh-config-rc-config-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - require:
      - sls: {{ sls_config_bootstrap }}
    - context:
        zsh: {{ stooj_dotfiles_common.zsh | json }}

{%- set rc_includes_dir = stooj_dotfiles_common.zsh.rc_includes_dir %}
stooj-dotfiles-common-zsh-config-rc-include-dir-managed:
  file.directory:
    - name: {{ stooj_dotfiles_common.zsh.zdotdir }}/{{ rc_includes_dir }}
    - user: stooj
    - group: stooj
    - mode: 755
    - makedirs: True
    - require:
      - sls: {{ sls_config_bootstrap }}

{% for name in stooj_dotfiles_common.zsh.rc_include_files %}
stooj-dotfiles-common-zsh-config-rc-include-{{ name }}-managed:
  file.managed:
    - name: {{ stooj_dotfiles_common.zsh.zdotdir }}/{{ rc_includes_dir }}/{{ name }}.zsh
    - source: {{ files_switch(['rc_includes/' ~ name ~ '.tmpl'],
                              lookup='stooj-dotfiles-common-zsh-config-rc-include-' ~ name ~ '-managed',
			      use_subpath=True
			     )
	      }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        zsh: {{ stooj_dotfiles_common.zsh | json }}
    - require:
      - stooj-dotfiles-common-zsh-config-rc-include-dir-managed
{% endfor %}

stooj-dotfiles-common-zsh-config-plugins-managed:
  file.managed:
    - name: {{ stooj_dotfiles_common.zsh.zdotdir }}/{{ stooj_dotfiles_common.zsh.plugins_file }}
    - source: {{ files_switch(['plugins.tmpl'],
                              lookup='stooj-dotfiles-common-zsh-config-plugins-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - require:
      - sls: {{ sls_config_bootstrap }}
    - context:
        zsh: {{ stooj_dotfiles_common.zsh | json }}

