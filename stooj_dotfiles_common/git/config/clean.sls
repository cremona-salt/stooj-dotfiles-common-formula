# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

stooj-dotfiles-common-git-config-clean-git-dir-absent:
  file.absent:
    - name: /home/stooj/.config/git

stooj-dotfiles-common-git-config-clean-stock-gitfile-absent:
  file.absent:
    - name: /home/stooj/.gitconfig

{% set home = '/home/stooj' %}
{% set exclude_file = stooj_dotfiles_common.git.config.get("core.excludesFile", '~/.gitignore')|replace('~', home) %}
stooj-dotfiles-common-git-config-clean-gitignore-file-absent:
  file.absent:
    - name: {{ exclude_file }}
