# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubroot = tplroot ~ '.' ~ tplsubname %}
{%- set sls_package_install = tplsubroot ~ '.package.install' %}
{%- set sls_user_present = tplroot ~ '.user.user' %}

include:
  - {{ sls_package_install }}
  - {{ sls_user_present }}


stooj-dotfiles-common-git-config-file-dir-managed:
  file.directory:
    - name: /home/stooj/.config/git
    - user: stooj
    - group: stooj
    - mode: 755
    - makedirs: True
    - require:
      - sls: {{ sls_user_present }}

stooj-dotfiles-common-git-config-file-managed:
  file.managed:
    - name: /home/stooj/.config/git/config
    - source: {{ files_switch(['gitconfig.tmpl'],
                              lookup='stooj-dotfiles-common-git-config-file-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        git: {{ stooj_dotfiles_common.git | json }}
    - require:
      - stooj-dotfiles-common-git-config-file-dir-managed
      - sls: {{ sls_user_present }}

{% set home = '/home/stooj' %}
{% set exclude_file = stooj_dotfiles_common.git.config.core.get("excludes_file", '~/.gitignore')|replace('~', home) %}
stooj-dotfiles-common-git-config-file-gitignore-managed:
  file.managed:
    - name: {{ exclude_file }}
    - source: {{ files_switch(['gitignore.tmpl'],
                              lookup='stooj-dotfiles-common-git-config-file-gitignore-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        git: {{ stooj_dotfiles_common.git | json }}
    - require:
      - stooj-dotfiles-common-git-config-file-dir-managed
      - sls: {{ sls_user_present }}
