# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubroot = tplroot ~ '.' ~ tplsubname %}
{#- Start imports as #}
{%- set sls_config_clean = tplsubroot ~ '.config.clean' %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

include:
  - {{ sls_config_clean }}

stooj-dotfiles-common-git-package-clean-pkg-removed:
  pkg.removed:
    - pkgs: {{ stooj_dotfiles_common.git.pkgs | yaml }}
    - require:
      - sls: {{ sls_config_clean }}
