# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

stooj-dotfiles-common-git-package-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles_common.git.pkgs | yaml }}
