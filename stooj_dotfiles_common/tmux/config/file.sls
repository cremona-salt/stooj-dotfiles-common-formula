# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set tplsubname = tpldir.split('/')[1] %}
{%- set tplsubroot = tplroot ~ '.' ~ tplsubname %}
{%- set sls_package_install = tplsubroot ~ '.package.install' %}
{%- set sls_user_present = tplroot ~ '.user.user' %}

include:
  - {{ sls_package_install }}
  - {{ sls_user_present }}

stooj-dotfiles-common-tmux-config-file-dir-managed:
  file.directory:
    - name: {{ stooj_dotfiles_common.tmux.config_dir }}
    - user: stooj
    - group: stooj
    - mode: 750
    - makedirs: True
    - require:
      - sls: {{ sls_user_present }}

stooj-dotfiles-common-tmux-config-file-config-managed:
  file.managed:
    - name: {{ stooj_dotfiles_common.tmux.config_dir }}/{{ stooj_dotfiles_common.tmux.config_file }}
    - source: {{ files_switch(['tmux.conf.tmpl'],
                              lookup='stooj-dotfiles-common-tmux-config-file-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        tmux: {{ stooj_dotfiles_common.tmux | json }}
    - require:
      - stooj-dotfiles-common-tmux-config-file-dir-managed
      - sls: {{ sls_user_present }}
