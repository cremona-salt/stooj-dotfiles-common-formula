# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

stooj-dotfiles-common-tmux-package-clean-tpm-absent:
  file.absent:
    - name: /home/stooj/.tmux/plugins/tpm

stooj-dotfiles-common-tmux-package-clean-stock-config-dir-absent:
  file.absent:
    - name: /home/stooj/.tmux

stooj-dotfiles-common-tmux-package-clean-package-absent:
  pkg.removed:
    - pkgs: {{ stooj_dotfiles_common.tmux.pkgs | yaml }}
