# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}
{%- set sls_git_package_install = tplroot ~ '.git.package.install' %}
{%- set sls_user_present = tplroot ~ '.user.user' %}

include:
  - {{ sls_git_package_install }}
  - {{ sls_user_present }}

stooj-dotfiles-common-tmux-package-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles_common.tmux.pkgs | yaml }}

stooj-dotfiles-common-tmux-package-install-stock-plugins-dir-managed:
  file.directory:
    - name: /home/stooj/.tmux/plugins
    - user: stooj
    - group: stooj
    - mode: 755
    - makedirs: True
    - require:
      - sls: {{ sls_user_present }}

stooj-dotfiles-common-tmux-package-install-tpm-installed:
  git.latest:
    - name: https://github.com/tmux-plugins/tpm
    - target: /home/stooj/.tmux/plugins/tpm
    - user: stooj
    - require:
      - stooj-dotfiles-common-tmux-package-install-stock-plugins-dir-managed
    - require:
      - sls: {{ sls_git_package_install }}
      - sls: {{ sls_user_present }}

{# tpm is cloned with ownership stooj:root #}
stooj-dotfiles-common-tmux-package-install-tpm-ownership-managed:
  file.directory:
    - name: /home/stooj/.tmux/plugins/tpm
    - user: stooj
    - group: stooj
    - recurse:
      - user
      - group
    - require:
      - stooj-dotfiles-common-tmux-package-install-tpm-installed
      - sls: {{ sls_user_present }}
