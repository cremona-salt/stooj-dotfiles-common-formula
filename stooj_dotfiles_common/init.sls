# -*- coding: utf-8 -*-
# vim: ft=sls

{% set os = salt.grains.get('os', None) %}
{% set osfamily = salt.grains.get('os_family', None) %}
{% set version = salt.grains.get('osrelease', None) %}
include:
  - .user
  - .build_essentials
  - .bind
  - .directories
  - .zsh
  - .git
  - .cig
  - .htop
  - .jq
  - .ncdu
  - .neovim
  - .tmux
  - .tree
  - .wget
  {%- if not(os == 'Ubuntu' and version == '18.04') %}
  {# fzf not available on 18.04 #}
  - .fzf
  {# Ripgrep not available on 18.04 #}
  - .ripgrep
  {%- endif %}
  {%- if osfamily == 'Arch' %}
  - .perl_rename
  {%- endif %}
