# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import stoojdotfilescommon with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

stoojdotfilescommon-config-file-file-managed:
  file.managed:
    - name: {{ stoojdotfilescommon.config }}
    - source: {{ files_switch(['example.tmpl'],
                              lookup='stoojdotfilescommon-config-file-file-managed'
                 )
              }}
    - mode: 644
    - user: root
    - group: {{ stoojdotfilescommon.rootgroup }}
    - makedirs: True
    - template: jinja
    - require:
      - sls: {{ sls_package_install }}
    - context:
        stoojdotfilescommon: {{ stoojdotfilescommon | json }}
