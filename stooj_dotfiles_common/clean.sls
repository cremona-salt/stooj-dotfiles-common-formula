# -*- coding: utf-8 -*-
# vim: ft=sls

{% set os = salt.grains.get('os', None) %}
{% set osfamily = salt.grains.get('os_family', None) %}
{% set version = salt.grains.get('osrelease', None) %}
include:
  - .directories.clean
  - .bind.clean
  - .zsh.clean
  - .git.clean
  - .htop.clean
  - .jq.clean
  - .ncdu.clean
  - .neovim.clean
  - .tmux.clean
  - .tree.clean
  - .wget.clean
  - .cig.clean
  {% if not(os == 'Ubuntu' and version == '18.04') %}
  {# Ripgrep not available on 18.04 #}
  - .ripgrep.clean
  {% endif %}
  {%- if osfamily == 'Arch' %}
  - .perl_rename.clean
  {%- endif %}
