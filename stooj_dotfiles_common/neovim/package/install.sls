# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}
{%- set sls_local_share_directory_create = tplroot ~ '.directories.local_share.directory' %}

include:
  - {{ sls_local_share_directory_create }}

stooj-dotfiles-common-neovim-package-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles_common.neovim.pkgs | yaml }}
    - require:
      - sls: {{ sls_local_share_directory_create }}
