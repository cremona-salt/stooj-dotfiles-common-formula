# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_clean = tplroot ~ '.neovim.config.clean' %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

include:
  - {{ sls_config_clean }}

stooj-dotfiles-common-neovim-package-clean-pkg-removed:
  pkg.removed:
    - names: {{ stooj_dotfiles_common.neovim.pkgs }}
    - require:
      - sls: {{ sls_config_clean }}

