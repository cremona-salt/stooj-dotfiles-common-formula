# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}
{%- set sls_local_share_directory_create = tplroot ~ '.directories.local_share.directory' %}
{%- set sls_user_present = tplroot ~ '.user.user' %}

include:
  - {{ sls_local_share_directory_create }}
  - {{ sls_user_present }}

stooj-dotfiles-common-neovim-config-file-plugin-dir-managed:
  file.directory:
    - name: /home/stooj/.local/share/nvim
    - user: stooj
    - group: stooj
    - mode: 755
    - makedirs: True
    - require:
      - sls: {{ sls_local_share_directory_create }}
      - sls: {{ sls_user_present }}

stooj-dotfiles-common-neovim-config-file-config-dir-managed:
  file.directory:
    - name: /home/stooj/.config/nvim
    - user: stooj
    - group: stooj
    - mode: 755
    - makedirs: True
    - require:
      - sls: {{ sls_user_present }}

stooj-dotfiles-common-neovim-config-file-init-managed:
  file.managed:
    - name: /home/stooj/.config/nvim/init.vim
    - source: {{ files_switch(['init.vim.tmpl'],
                              lookup='stooj-dotfiles-common-neovim-config-file-init-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        neovim: {{ stooj_dotfiles_common.neovim | json }}
    - require:
      - stooj-dotfiles-common-neovim-config-file-config-dir-managed
      - sls: {{ sls_user_present }}

stooj-dotfiles-common-neovim-config-file-bundles-managed:
  file.managed:
    - name: /home/stooj/.config/nvim/bundles.vim
    - source: {{ files_switch(['bundles.vim.tmpl'],
                              lookup='stooj-dotfiles-common-neovim-config-file-bundles-managed',
                              use_subpath=True
                             )
              }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        neovim: {{ stooj_dotfiles_common.neovim | json }}
    - require:
      - stooj-dotfiles-common-neovim-config-file-config-dir-managed
      - sls: {{ sls_user_present }}

{%- for name in stooj_dotfiles_common.neovim.after_ftdetect_files %}
stooj-dotfiles-common-neovim-config-after-ftdetect-{{ name }}-managed:
  file.managed:
    - name: /home/stooj/.config/nvim/after/ftdetect/{{ name }}.vim
    - source: {{ files_switch(['after/ftdetect/' ~ name ~ '.vim.tmpl'],
                              lookup='stooj-dotfiles-common-neovim-config-after-ftdetect-' ~ name ~ '-managed',
			      use_subpath=True
			     )
	      }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        neovim: {{ stooj_dotfiles_common.neovim | json }}
    - require:
      - sls: {{ sls_user_present }}

{%- endfor %}

{%- for name in stooj_dotfiles_common.neovim.after_ftplugin_files %}
stooj-dotfiles-common-neovim-config-after-ftplugin-{{ name }}-managed:
  file.managed:
    - name: /home/stooj/.config/nvim/after/ftplugin/{{ name }}.vim
    - source: {{ files_switch(['after/ftplugin/' ~ name ~ '.vim.tmpl'],
                              lookup='stooj-dotfiles-common-neovim-config-after-ftplugin-' ~ name ~ '-managed',
			      use_subpath=True
			     )
	      }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        neovim: {{ stooj_dotfiles_common.neovim | json }}
    - require:
      - sls: {{ sls_user_present }}
{%- endfor %}

{%- for name in stooj_dotfiles_common.neovim.after_indent_files %}
stooj-dotfiles-common-neovim-config-after-indent-{{ name }}-managed:
  file.managed:
    - name: /home/stooj/.config/nvim/after/indent/{{ name }}.vim
    - source: {{ files_switch(['after/indent/' ~ name ~ '.vim.tmpl'],
                              lookup='stooj-dotfiles-common-neovim-config-after-indent-' ~ name ~ '-managed',
			      use_subpath=True
			     )
	      }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        neovim: {{ stooj_dotfiles_common.neovim | json }}
    - require:
      - sls: {{ sls_user_present }}
{%- endfor %}

{%- for name in stooj_dotfiles_common.neovim.plugin_files %}
stooj-dotfiles-common-neovim-config-plugin-{{ name }}-managed:
  file.managed:
    - name: /home/stooj/.config/nvim/plugin/{{ name }}.vim
    - source: {{ files_switch(['plugin/' ~ name ~ '.vim.tmpl'],
                              lookup='stooj-dotfiles-common-neovim-config-plugin-' ~ name ~ '-managed',
			      use_subpath=True
			     )
	      }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        neovim: {{ stooj_dotfiles_common.neovim | json }}
    - require:
      - sls: {{ sls_user_present }}
{%- endfor %}

{%- for name in stooj_dotfiles_common.neovim.snippets_files %}
stooj-dotfiles-common-neovim-config-snippets-{{ name }}-managed:
  file.managed:
    - name: /home/stooj/.config/nvim/ultisnippets/{{ name }}.snippets
    - source: {{ files_switch(['ultisnippets/' ~ name ~ '.snippets.tmpl'],
                              lookup='stooj-dotfiles-common-neovim-config-snippets-' ~ name ~ '-managed',
			      use_subpath=True
			     )
	      }}
    - mode: 644
    - user: stooj
    - group: stooj
    - makedirs: True
    - template: jinja
    - context:
        neovim: {{ stooj_dotfiles_common.neovim | json }}
    - require:
      - sls: {{ sls_user_present }}
{%- endfor %}
