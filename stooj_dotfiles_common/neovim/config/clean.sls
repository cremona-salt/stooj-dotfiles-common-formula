# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import stooj_dotfiles_common with context %}

{% for name in stooj_dotfiles_common.neovim.after_indent_files %}
stooj-dotfiles-common-neovim-config-clean-after-indent-{{ name }}-absent:
  file.absent:
    - name: /home/stooj/.config/nvim/after/indent/{{ name }}.vim
{% endfor %}

{% for name in stooj_dotfiles_common.neovim.after_ftplugin_files %}
stooj-dotfiles-common-neovim-config-clean-after-ftplugin-{{ name }}-absent:
  file.absent:
    - name: /home/stooj/.config/nvim/after/ftplugin/{{ name }}.vim
{% endfor %}

{% for name in stooj_dotfiles_common.neovim.after_ftdetect_files %}
stooj-dotfiles-common-neovim-config-clean-after-ftdetect-{{ name }}-absent:
  file.absent:
    - name: /home/stooj/.config/nvim/after/ftdetect/{{ name }}.vim
{% endfor %}

stooj-dotfiles-common-neovim-config-clean-bundles-absent:
  file.absent:
    - name: /home/stooj/.config/nvim/bundles.vim

stooj-dotfiles-common-neovim-config-clean-init-absent:
  file.absent:
    - name: /home/stooj/.config/nvim/init.vim

stooj-dotfiles-common-neovim-config-clean-config-dir-absent:
  file.absent:
    - name: /home/stooj/.config/nvim

stooj-dotfiles-common-neovim-config-clean-plugin-dir-absent:
  file.absent:
    - name: /home/stooj/.local/share/nvim
