# frozen_string_literal: true

packages = ['neovim', 'python-neovim', 'python3-neovim']
packages = ['neovim', 'python-pynvim'] if (os[:name] == 'arch')

packages.each do |pkg|
  control 'stooj_dotfiles_common neovim package' do
    title pkg + ' should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
