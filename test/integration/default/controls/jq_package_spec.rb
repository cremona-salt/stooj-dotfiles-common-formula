# frozen_string_literal: true

control 'stooj_dotfiles_common jq package' do
  title 'should be installed'

  describe package('jq') do
    it { should be_installed }
  end
end
