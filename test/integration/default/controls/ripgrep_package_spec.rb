# frozen_string_literal: true

unless os[:name] == 'ubuntu' and os[:release] == '18.04' then
  control 'stooj_dotfiles_common ripgrep package' do
    title 'ripgrep should be installed'

    describe package('ripgrep') do
      it { should be_installed }
    end
  end
end
