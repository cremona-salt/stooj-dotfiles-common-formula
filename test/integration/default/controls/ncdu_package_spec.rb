# frozen_string_literal: true

control 'stooj_dotfiles_common ncdu package' do
  title 'should be installed'

  describe package('ncdu') do
    it { should be_installed }
  end
end
