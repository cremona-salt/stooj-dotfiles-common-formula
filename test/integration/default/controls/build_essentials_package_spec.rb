# frozen_string_literal: true

packages = ['dpkg-dev', 'g++', 'gcc', 'libc6-dev', 'make']
packages = [
    'autoconf',
    'automake',
    'binutils',
    'bison',
    'fakeroot',
    'file',
    'findutils',
    'flex',
    'gawk',
    'gcc',
    'gettext',
    'grep',
    'groff',
    'gzip',
    'libtool',
    'm4',
    'make',
    'pacman',
    'patch',
    'pkgconf',
    'sed',
    'sudo',
    'texinfo',
    'which'
] if (os[:name] == 'arch')

packages.each do |pkg|
  control 'stooj_dotfiles_common build_essentials ' + pkg + ' package' do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end

