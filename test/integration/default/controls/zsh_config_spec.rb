# frozen_string_literal: true

control 'stooj_dotfiles_common zsh env entry configuration' do
  title 'should match desired lines'

  describe file('/home/stooj/.zshenv') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('ZDOTDIR=/home/stooj/.config/zsh') }
    its('content') { should include('. $ZDOTDIR/.zshenv') }
  end
end

control 'stooj_dotfiles_common zdotdir created' do
  title 'should be created'

  describe directory('/home/stooj/.config/zsh') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end

control 'stooj_dotfiles_common .local/share/zsh created' do
  title 'should be created'

  describe directory('/home/stooj/.local/share/zsh') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end

control 'stooj_dotfiles_common zprofile created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/.zprofile') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('ZDOTDIR=/home/stooj/.config/zsh') }
    its('content') { should include('. $ZDOTDIR/.zshenv') }
  end
end

control 'stooj_dotfiles_common zshenv_functions created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/.zenv_functions') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Usage: indirect_expand PATH -> $PATH') }
  end
end

control 'stooj_dotfiles_common zshenv created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/.zshenv') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('. /home/stooj/.config/zsh/.zenv_functions') }
  end
end

control 'stooj_dotfiles_common zshrc created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/.zshrc') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Enable autocompletion') }
    its('content') { should include('source <(antibody init)') }
    its('content') { should include('for file in /home/stooj/.config/zsh/rc_includes/*; do') }
  end
end

control 'stooj_dotfiles_common zlogout created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/.zlogout') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') do
      should include('# when leaving the console, clear the screen to increase privacy') 
    end
  end
end

control 'stooj_dotfiles_common env_includes created' do
  title 'should be created'

  describe directory('/home/stooj/.config/zsh/env_includes') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end

control 'stooj_dotfiles_common env_includes/editor created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/env_includes/editor.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Preferred programmes') }
  end
end

control 'stooj_dotfiles_common env_includes/pager created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/env_includes/pager.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Preferred pager') }
  end
end

control 'stooj_dotfiles_common env_includes/personal-details created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/env_includes/personal-details.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('export NAME=\'josephus miller\'') }
  end
end

control 'stooj_dotfiles_common env_includes/localization created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/env_includes/localization.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Language') }
    its('content') { should include('export LANG=\'en_GB.UTF-8\'') }
    its('content') { should include('export LC_ALL=$LANG') }
    its('content') { should include('export LC_COLLATE=$LANG') }
    its('content') { should include('export LC_CTYPE=$LANG') }
    its('content') { should include('export LC_MESSAGES=$LANG') }
    its('content') { should include('export LC_MONETARY=$LANG') }
    its('content') { should include('export LC_NUMERIC=$LANG') }
    its('content') { should include('export LC_TIME=$LANG') }
  end
end

control 'stooj_dotfiles_common env_includes/xdg created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/env_includes/xdg.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Set XDG directories') }
    its('content') { should include('XDG_CONFIG_HOME=${HOME}/.config') }
    its('content') { should include('export XDG_CONFIG_HOME') }
    its('content') { should include('XDG_CACHE_HOME=${HOME}/.cache') }
    its('content') { should include('export XDG_CACHE_HOME') }
    its('content') { should include('XDG_DATA_HOME=${HOME}/.local/share') }
    its('content') { should include('export XDG_DATA_HOME') }
  end
end

control 'stooj_dotfiles_common rc_includes created' do
  title 'should be created'

  describe directory('/home/stooj/.config/zsh/rc_includes') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end

control 'stooj_dotfiles_common rc_includes/history created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/rc_includes/history.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Configure history') }
    its('content') { should include('HISTFILE="/home/stooj/.local/share/zsh/history') }
    its('content') { should include('HISTSIZE=10000') }
    its('content') { should include('SAVEHIST=HISTSIZE') }
  end
end

control 'stooj_dotfiles_common rc_includes/tmux created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/rc_includes/tmux.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Test to see if tmux plugins are installed') }
    its('content') { should include('if [ "$TMUX" ]; then') }
  end
end

control 'stooj_dotfiles_common rc_includes/vimmode created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/rc_includes/vimmode.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Press ESC v to edit your command in your $EDITOR') }
  end
end

control 'stooj_dotfiles_common rc_includes/comments created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/rc_includes/comments.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Allow comments in the shell') }
  end
end

control 'stooj_dotfiles_common rc_includes/aliases-common created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/rc_includes/aliases-common.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('alias ls=\'ls --color=auto --group-directories-first\'') }
  end
end

control 'stooj_dotfiles_common plugins file created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/plugins.txt') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('zsh-users/zsh-completions') }
  end
end
