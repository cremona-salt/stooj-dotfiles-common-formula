# frozen_string_literal: true

control 'stooj_dotfiles_common tree package' do
  title 'tree should be installed'

  describe package('tree') do
    it { should be_installed }
  end
end
