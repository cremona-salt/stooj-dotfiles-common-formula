# frozen_string_literal: true

control 'stooj_dotfiles_common cig config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.cig.yaml') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0600' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('code: /home/stooj/code') }
  end
end
