# frozen_string_literal: true

control 'stooj_dotfiles_common git ignore file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/git/gitignore') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('# Anything with a tilda at the end is a temporary file or something.') }
  end
end

control 'stooj_dotfiles_common git config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/git/config') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('[user]') }
    its('content') { should include('	email = jmiller@ceres.station') }
    its('content') { should include('	name = josephus miller') }
    its('content') { should include('[difftool "vimdiff3"]') }
    its('content') { should include('	path = nvim') }
  end
end

control 'stooj_dotfiles_common git config dir' do
  title 'should be created'

  describe directory('/home/stooj/.config/git') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end

