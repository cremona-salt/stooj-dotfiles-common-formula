# frozen_string_literal: true

control 'stooj_dotfiles_common nvim plugin dir created' do
  title 'should be created'

  describe directory('/home/stooj/.local/share/nvim') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end

control 'stooj_dotfiles_common nvim config dir created' do
  title 'should be created'

  describe directory('/home/stooj/.config/nvim') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end

control 'stooj_dotfiles_common nvim config init created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/nvim/init.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('  set nocompatible') }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config bundles created' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/nvim/bundles.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
    its('content') { should include('call dein#add(\'tpope/vim-fugitive\')') }
  end
end

control 'stooj_dotfiles_common nvim config after ftdetect csv created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftdetect/csv.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftdetect rst created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftdetect/rst.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftdetect sls created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftdetect/sls.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftplugin csv created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftplugin/csv.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftplugin c created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftplugin/c.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftplugin dot created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftplugin/dot.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftplugin go created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftplugin/go.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftplugin javascript created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftplugin/javascript.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftplugin markdown created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftplugin/markdown.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftplugin perl created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftplugin/perl.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftplugin python created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftplugin/python.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftplugin rst created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftplugin/rst.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftplugin rtv created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftplugin/rtv.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftplugin ruby created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftplugin/ruby.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftplugin sh created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftplugin/sh.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftplugin text created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftplugin/text.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftplugin tex created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftplugin/tex.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after ftplugin yaml created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/ftplugin/yaml.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after indent go created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/indent/go.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after indent html handlebars created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/indent/html.handlebars.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after indent markdown created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/indent/markdown.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after indent rst created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/indent/rst.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

control 'stooj_dotfiles_common nvim config after indent ruby created' do
  title 'should exist'

  describe file('/home/stooj/.config/nvim/after/indent/ruby.vim') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('" Your changes will be overwritten.') }
  end
end

snippets = ['all', 'c', 'go', 'htmldjango', 'html', 'markdown', 'moin', 'php',
            'python', 'sh', 'todo', 'vim']
snippets.each do |snippet|
  control 'stooj_dotfiles_common nvim config snippets ' + snippet do
    title 'should exist'
  
    describe file('/home/stooj/.config/nvim/ultisnippets/' + snippet + '.snippets') do
      it { should be_file }
      it { should be_owned_by 'stooj' }
      it { should be_grouped_into 'stooj' }
      its('mode') { should cmp '0644' }
      its('content') { should include('" Your changes will be overwritten.') }
    end
  end
end
