# frozen_string_literal: true

control 'stooj_dotfiles_common directory bin created' do
  title 'should be created'

  describe directory('/home/stooj/bin') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end

control 'stooj_dotfiles_common directory .local/share created' do
  title 'should be created'

  describe directory('/home/stooj/.local/share') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end
