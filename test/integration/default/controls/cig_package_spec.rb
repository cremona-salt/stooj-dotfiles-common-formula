# frozen_string_literal: true

control 'stooj_dotfiles_common cig package' do
  title 'cig should be installed'

  describe file('/home/stooj/bin/cig') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0700' }
  end
end
