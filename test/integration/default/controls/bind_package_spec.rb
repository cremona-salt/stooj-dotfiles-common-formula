# frozen_string_literal: true

package = 'dnsutils'
package = 'bind' if os[:name] == 'arch')

control 'stooj_dotfiles_common ' + pkg + ' package' do
  title 'should be installed'

  describe package(package) do
    it { should be_installed }
  end
end
