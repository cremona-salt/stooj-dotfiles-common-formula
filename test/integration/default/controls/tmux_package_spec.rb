# frozen_string_literal: true

control 'stooj_dotfiles_common tmux package' do
  title 'tmux should be installed'

  describe package('tmux') do
    it { should be_installed }
  end
end

control 'stooj_dotfiles_common tmux stock config dir' do
  title 'should be created'

  describe directory('/home/stooj/.tmux/plugins') do
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end

control 'stooj_dotfiles_common tmux tpm' do
  title 'should be installed'

  describe file('/home/stooj/.tmux/plugins/tpm/tpm') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end
