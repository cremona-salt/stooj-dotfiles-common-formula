# frozen_string_literal: true

packages = ['htop', 'lsof', 'strace']
packages.each do |pkg|
  control 'stooj_dotfiles_common ' + pkg + ' package' do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
