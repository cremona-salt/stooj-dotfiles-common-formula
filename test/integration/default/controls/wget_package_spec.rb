# frozen_string_literal: true

control 'stooj_dotfiles_common wget package' do
  title 'should be installed'

  describe package('wget') do
    it { should be_installed }
  end
end
