# frozen_string_literal: true

control 'stooj_dotfiles_common user stooj' do
  title 'user should be present'

  describe user('stooj') do
  it { should exist }
  its('shell') { should eq '/usr/bin/zsh' }
  end
end
