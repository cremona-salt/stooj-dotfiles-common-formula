# frozen_string_literal: true

control 'stooj_dotfiles_common zsh package' do
  title 'should be installed'

  describe package('zsh') do
    it { should be_installed }
  end
end

if os[:family] == 'Arch' then
  control 'stooj_dotfiles_common zsh-completions package' do
    title 'should be installed'

    describe package('zsh-completions') do
      it { should be_installed }
    end
  end
end

control 'stooj_dotfiles_common zsh antibody' do
  title 'antibody should be installed'

  describe file('/home/stooj/.local/share/antibody/antibody') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0755' }
  end
end

control 'stooj_dotfiles_common zsh antibody symlink' do
  title 'antibody should be symlinked in bin'

  describe file('/home/stooj/bin/antibody') do
      it { should be_symlink }
      it { should_not be_directory }
      its('link_path') { should eq '/home/stooj/.local/share/antibody/antibody' }
  end
end
