# frozen_string_literal: true

unless os[:name] == 'ubuntu' and os[:release] == '18.04' then
  control 'stooj_dotfiles_common fzf package' do
    title 'fzf should be installed'

    describe package('fzf') do
      it { should be_installed }
    end
  end
end
