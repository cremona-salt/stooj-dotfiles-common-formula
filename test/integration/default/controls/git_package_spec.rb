# frozen_string_literal: true

control 'stooj_dotfiles_common git package' do
  title 'git should be installed'

  describe package('git') do
    it { should be_installed }
  end
end
