# frozen_string_literal: true

control 'stooj_dotfiles_common cig config file' do

  title 'should not exist'
  describe file('/home/stooj/.cig.yaml') do
    it { should_not exist }
  end
end
