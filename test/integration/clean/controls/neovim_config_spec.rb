# frozen_string_literal: true

control 'stooj_dotfiles_common nvim dirs should not exist' do
  title 'nvim directories should not exist'

  describe directory('/home/stooj/.local/share/nvim') do
    it { should_not exist }
  end
  describe directory('/home/stooj/.config/nvim') do
    it { should_not exist }
  end
  describe directory('/home/stooj/.config/nvim/after/ftdetect') do
    it { should_not exist }
  end
  describe directory('/home/stooj/.config/nvim/after/ftplugin') do
    it { should_not exist }
  end
  describe directory('/home/stooj/.config/nvim/after/indent') do
    it { should_not exist }
  end
end

ftdetect_files = ['csv', 'rst', 'sls']
ftplugin_files = [
  'csv', 'c', 'dot', 'go', 'javascript', 'markdown', 'perl', 'python', 'rst',
  'rtv', 'ruby', 'sh', 'text', 'tex', 'yaml']
indent_files = ['go', 'html.handlebars', 'markdown', 'rst', 'ruby']
snippets_files = ['all', 'c', 'go', 'htmldjango', 'html', 'markdown', 'moin',
                  'php', 'python', 'sh', 'todo', 'vim']
control 'stooj_dotfiles_common nvim files should not exist' do
  title 'nvim files should not exist'

  describe file('/home/stooj/.config/nvim/init.vim') do
    it { should_not exist }
  end
  describe file('/home/stooj/.config/nvim/bundles.vim') do
    it { should_not exist }
  end

  ftdetect_files.each do |vimfile|
    describe file('/home/stooj/.config/nvim/after/ftdetect/' + vimfile + '.vim') do
      it { should_not exist }
    end
  end
  ftplugin_files.each do |vimfile|
    describe file('/home/stooj/.config/nvim/after/ftplugin/' + vimfile + '.vim') do
      it { should_not exist }
    end
  end
  indent_files.each do |vimfile|
    describe file('/home/stooj/.config/nvim/after/indent/' + vimfile + '.vim') do
      it { should_not exist }
    end
  end
  snippets_files.each do |vimfile|
    describe file('/home/stooj/.config/nvim/ultisnippets/' + vimfile + '.snippets') do
      it { should_not exist }
    end
  end

end
