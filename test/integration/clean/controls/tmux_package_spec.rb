# frozen_string_literal: true

control 'stooj_dotfiles_common tmux tpm' do
  title 'should not exist'

  describe file('/home/stooj/.tmux/plugins/tpm/tpm') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_common tmux stock config dir' do
  title 'should not exist'
  describe directory('/home/stooj/.tmux/plugins') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_common tmux package' do
  title 'should not be installed'
  describe package('tmux') do
    it { should_not be_installed }
  end
end
