# frozen_string_literal: true

control 'stooj_dotfiles_common fzf package' do
  title 'should not be installed'
  describe package('fzf') do
    it { should_not be_installed }
  end
end

