# frozen_string_literal: true
#
control 'stooj_dotfiles_common jq package' do
  title 'should not be installed'
  describe package('jq') do
    it { should_not be_installed }
  end
end
