# frozen_string_literal: true

control 'stooj_dotfiles_common git config dir' do
  title 'should not exist'

  describe directory('/home/stooj/.config/git') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_common git config file' do
  title 'should not exist'
  describe file('/home/stooj/.config/git/config') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_common git ignore file' do
  title 'should not exist'
  describe file('/home/stooj/.config/git/gitignore') do
    it { should_not exist }
  end
end
