# frozen_string_literal: true

control 'stooj_dotfiles_common directory bin removed' do
  title 'the bin dir should not exist'

  describe directory('/home/stooj/bin') do
    it { should_not exist }
  end
end
