# frozen_string_literal: true

control 'stooj_dotfiles_common tmux config dir' do
  title 'should not exist'

  describe directory('/home/stooj/.config/tmux') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_common tmux config file' do
  title 'should not exist'
  describe file('/home/stooj/.config/tmux/tmux.conf') do
    it { should_not exist }
  end
end
