# frozen_string_literal: true

control 'stooj_dotfiles_common zsh antibody symlink' do
  title 'should not exist'

  describe file('/home/stooj/bin/antibody') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_common zsh antibody' do
  title 'should not exist'
  describe directory('/home/stooj/.local/share/antibody') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_common zsh package' do
  title 'should not be installed'
  describe package('zsh') do
    it { should_not be_installed }
  end
end
