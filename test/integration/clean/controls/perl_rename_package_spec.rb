# frozen_string_literal: true

control 'stooj_dotfiles_common perl-rename package' do
  title 'should not be installed'
  describe package('perl-rename') do
    it { should_not be_installed }
  end
end
