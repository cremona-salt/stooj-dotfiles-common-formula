# frozen_string_literal: true

control 'stooj_dotfiles_common perl-rename rc_includes file' do
  title 'should not exist'
  describe file('/home/stooj/.config/zsh/rc_includes/perl-rename.zsh') do
    it { should_not exist }
  end
end
