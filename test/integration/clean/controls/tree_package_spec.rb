# frozen_string_literal: true

control 'stooj_dotfiles_common tree package' do
  title 'should not be installed'
  describe package('tree') do
    it { should_not be_installed }
  end
end
