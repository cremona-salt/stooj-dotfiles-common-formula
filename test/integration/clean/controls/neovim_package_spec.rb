# frozen_string_literal: true

control 'stooj_dotfiles_common neovim package removed' do
  title 'neovim must be uninstalled'

  describe package('nvim') do
    it { should_not be_installed }
  end
end
