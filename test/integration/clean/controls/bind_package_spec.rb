# frozen_string_literal: true

package = 'dnsutils'
package = 'bind' if os[:name] == 'arch')

control 'stooj_dotfiles_common ' + pkg + ' package' do
  title 'should not be installed'

  describe package(package) do
    it { should_not be_installed }
  end
end

