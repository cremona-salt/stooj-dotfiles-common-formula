# frozen_string_literal: true
#
control 'stooj_dotfiles_common wget package' do
  title 'should not be installed'
  describe package('wget') do
    it { should_not be_installed }
  end
end
