# frozen_string_literal: true

control 'stooj_dotfiles_common cig package' do
  title 'should be absent'

  describe file('/home/stooj/bin/cig') do
    it { should_not exist }
  end
end
