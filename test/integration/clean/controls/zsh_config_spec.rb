# frozen_string_literal: true

control 'stooj_dotfiles_common plugins file' do
  title 'should not exist'

  describe file('/home/stooj/.config/zsh/plugins.txt') do
    it { should_not exist }
  end
end

rc_includes_files = ['history', 'tmux', 'vimmode', 'comments', 'aliases-common']

control 'stooj_dotfiles_common rc_includes files' do
  title 'should not exist'

  rc_includes_files.each do |incfile|
    describe file('/home/stooj/.config/zsh/rc_includes/' + incfile + '.zsh') do
      it { should_not exist }
    end
  end
end

control 'stooj_dotfiles_common rc_includes directory' do
  title 'should not exist'
  describe directory('/home/stooj/.config/zsh/rc_includes') do
    it { should_not exist }
  end
end

env_includes_files = ['localization', 'personal-details', 'pager', 'editor', 'xdg']
control 'stooj_dotfiles_common env_includes files' do
  title 'should not exist'

  env_includes_files.each do |incfile|
    describe file('/home/stooj/.config/zsh/env_includes/' + incfile + '.zsh') do
      it { should_not exist }
    end
  end
end

control 'stooj_dotfiles_common env_includes directory' do
  title 'should not exist'
  describe directory('/home/stooj/.config/zsh/env_includes') do
    it { should_not exist }
  end
end

conf_files = ['.zlogout', '.zshrc', '.zshenv', '.zsenv_functions', '.zprofile']
control 'stooj_dotfiles_common conf files' do
  title 'should not exist'

  conf_files.each do |incfile|
    describe file('/home/stooj/.config/zsh/' + incfile) do
      it { should_not exist }
    end
  end
end

control 'stooj_dotfiles_common .config/zsh directory' do
  title 'should not exist'
  describe directory('/home/stooj/.config/zsh') do
    it { should_not exist }
  end
end

control 'stooj_dotfiles_common env entry file' do
  title 'should not exist'
  describe file('/home/stooj/.zshenv') do
    it { should_not exist }
  end
end
