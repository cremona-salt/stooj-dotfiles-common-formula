# frozen_string_literal: true
#
control 'stooj_dotfiles_common ripgrep package' do
  title 'should not be installed'
  describe package('ripgrep') do
    it { should_not be_installed }
  end
end
