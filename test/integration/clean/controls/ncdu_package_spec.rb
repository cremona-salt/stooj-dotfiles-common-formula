# frozen_string_literal: true
#
control 'stooj_dotfiles_common ncdu package' do
  title 'should not be installed'
  describe package('ncdu') do
    it { should_not be_installed }
  end
end
