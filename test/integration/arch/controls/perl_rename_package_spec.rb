# frozen_string_literal: true

control 'stooj_dotfiles_common perl-rename package' do
  title 'perl-rename should be installed'

  describe package('perl-rename') do
    it { should be_installed }
  end
end
