# frozen_string_literal: true

control 'stooj_dotfiles_common perl-rename config file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/zsh/rc_includes/perl-rename.zsh') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('command -v perl-rename > /dev/null &&') }
  end
end
