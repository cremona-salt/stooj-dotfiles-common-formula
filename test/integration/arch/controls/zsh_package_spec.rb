# frozen_string_literal: true

control 'stooj_dotfiles_common zsh package' do
  title 'zsh should be installed'

  describe package('zsh') do
    it { should be_installed }
  end
end

control 'stooj_dotfiles_common zsh-completions' do
  title 'zsh-completions should be installed'

  describe package('zsh') do
    it { should be_installed }
  end
end
